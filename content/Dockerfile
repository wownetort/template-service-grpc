FROM microsoft/dotnet:2.2-runtime as base
WORKDIR /app

FROM microsoft/dotnet:2.2-sdk AS build
RUN GRPC_HEALTH_PROBE_VERSION=v0.1.0-alpha.1 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64
WORKDIR /src

# copy nuget configuration for private nuget feed
COPY NuGet.config /root/.nuget/NuGet/NuGet.Config

# copy csproj and restore as distinct layers
COPY src/*/*.csproj ./
RUN for file in $(ls *.csproj); \ 
	do mkdir -p ${file%.*}/ \ 
	&& mv $file ${file%.*}/; done

RUN dotnet restore Templates.Grpc/Templates.Grpc.csproj

# copy and build everithing else
COPY src/. ./
RUN dotnet build --no-restore --configuration Release Templates.Grpc/Templates.Grpc.csproj

# publish result
FROM build as publish
WORKDIR /src/Templates.Grpc
RUN dotnet publish --no-restore --no-build --configuration Release --output out

# copy output to separate base image for runtime
FROM base as final
COPY --from=publish /src/Templates.Grpc/out ./
COPY --from=build /bin/grpc_health_probe /bin/
RUN chmod +x /bin/grpc_health_probe

ENTRYPOINT [ "dotnet", "Templates.Grpc.dll" ]