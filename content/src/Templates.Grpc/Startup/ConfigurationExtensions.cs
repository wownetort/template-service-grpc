using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Templates.Grpc.Startup
{
	public static class ConfigurationExtensions
	{
		public static IHostBuilder ConfigureHostConfiguration(this IHostBuilder hostBuilder)
		{
			return hostBuilder.ConfigureHostConfiguration(builder => builder
				.AddEnvironmentVariables(prefix: "GRPC_"));
		}

		public static IHostBuilder ConfigureAppConfiguration(this IHostBuilder hostBuilder)
		{
			return hostBuilder.ConfigureAppConfiguration((context, builder) => builder
				.AddJsonFile("appsettings.json", optional: false)
				.AddJsonFile($"appsettings.{context.HostingEnvironment.EnvironmentName}.json", optional: true)
				.AddEnvironmentVariables());
		}
	}
}