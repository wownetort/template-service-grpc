using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Templates.Grpc.Configuration;
using Grpc.Core;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Prometheus;

namespace Templates.Grpc.Startup
{
	public static class HostingExtensions
	{
		public static IHostBuilder UseMetrics(this IHostBuilder hostBuilder)
		{
			return hostBuilder.ConfigureServices((context, services) =>
				services.AddSingleton<IHostedService, MetricsService>());
		}

		public static IHostBuilder UseGrpcListener(this IHostBuilder hostBuilder)
		{
			return hostBuilder.ConfigureServices((context, services) =>
				services.AddSingleton<IHostedService, GrpcHostedService>());
		}

		private sealed class MetricsService : IHostedService
		{
			private readonly ILogger<MetricsService> _logger;
			private readonly MetricServer _metricsServer;
			private readonly string _host;
			private readonly int _port;

			public MetricsService(
				IOptions<EnvironmentConfiguration> provider,
				ILogger<MetricsService> logger)
			{
				_host = provider.Value.MetricsServerHost;
				_port = provider.Value.MetricsServerPort;
				_metricsServer = new MetricServer(_host, _port);
				_logger = logger;
			}

			public Task StartAsync(CancellationToken cancellationToken)
			{
				_logger.LogInformation("Starting metrics server on {Host}:{Port}", _host, _port);
				_metricsServer.Start();
				_logger.LogInformation("Started metrics server on {Host}:{Port}", _host, _port);
				return Task.CompletedTask;
			}

			public Task StopAsync(CancellationToken cancellationToken)
			{
				_logger.LogInformation("Stopping metrics server");
				_metricsServer.Stop();
				_logger.LogInformation("Stopped metrics server");
				return Task.CompletedTask;
			}
		}

		private sealed class GrpcHostedService : IHostedService
		{
			private readonly IEnumerable<ServerServiceDefinition> _definitions;
			private readonly EnvironmentConfiguration _configuration;
			private readonly ILogger<GrpcHostedService> _logger;
			private readonly Lazy<Server> _server;

			public GrpcHostedService(
				IEnumerable<ServerServiceDefinition> definitions,
				IOptions<EnvironmentConfiguration> options,
				ILogger<GrpcHostedService> logger)
			{
				_definitions = definitions;
				_configuration = options.Value;
				_logger = logger;
				_server = new Lazy<Server>(CreateGrpcServer);
			}

			public Task StartAsync(CancellationToken cancellationToken)
			{
				_logger.LogInformation("Starting app server on {Host}:{Port}", _configuration.Host, _configuration.Port);
				_server.Value.Start();
				_logger.LogInformation("Started app server on {Host}:{Port}", _configuration.Host, _configuration.Port);
				return Task.CompletedTask;
			}

			public async Task StopAsync(CancellationToken cancellationToken)
			{
				if (!_server.IsValueCreated)
				{
					_logger.LogInformation("App server not started");
					return;
				}

				_logger.LogInformation("Stopping app server");
				await _server.Value.ShutdownAsync();
				_logger.LogInformation("Stopped app server");
			}

			private Server CreateGrpcServer()
			{
				var server = new Server
				{
					Ports = { { _configuration.Host, _configuration.Port, ServerCredentials.Insecure } }
				};

				foreach (var definition in _definitions)
				{
					server.Services.Add(definition);
				}

				return server;
			}
		}
	}
}