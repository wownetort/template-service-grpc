using System.Text;
using Templates.Grpc.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Configuration;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using Serilog.Sinks.Prometheus;

namespace Templates.Grpc.Startup
{
	public static class LoggingExtensions
	{
		public static IHostBuilder ConfigureAppLogging(this IHostBuilder hostBuilder)
		{
			return hostBuilder.ConfigureLogging(Configure);
		}

		private static void Configure(HostBuilderContext context, ILoggingBuilder builder)
		{
			var config = context.Configuration.Get<EnvironmentConfiguration>();
			var logger = new LoggerConfiguration()
				.MinimumLevel.Is(config.LogLevel)
				.WriteTo.Console(config)
				.WriteTo.Prometheus($"{config.PrometheusName}_{{0}}", "Total count events of level {0}")
				.CreateLogger();

			builder.AddSerilog(logger);
		}

		private static LoggerConfiguration Console(this LoggerSinkConfiguration sinkConfiguration,
			EnvironmentConfiguration configuration)
		{
			System.Console.OutputEncoding = Encoding.UTF8;

			return configuration.LogsAtJsonFormat
				? sinkConfiguration.Console(
					standardErrorFromLevel: LogEventLevel.Error,
					formatter: new ExceptionAsObjectJsonFormatter(renderMessage: true))
				: sinkConfiguration.Console(
					standardErrorFromLevel: LogEventLevel.Error);
		}
	}
}