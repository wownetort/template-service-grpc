using System;
using System.Linq;
using Templates.Grpc.Configuration;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Templates.Grpc.Startup
{
	public static class ServicesExtensions
	{
		public static IHostBuilder ConfigureAppServices(this IHostBuilder hostBuilder)
		{
			return hostBuilder.ConfigureServices(Configure);
		}

		private static void Configure(HostBuilderContext context, IServiceCollection services)
		{
		    services
		        .AddAppOptions(context.Configuration);
		}

		private static IServiceCollection AddAppOptions(this IServiceCollection services, IConfiguration configuration)
		{
			services.AddOptions();
			services.Configure<EnvironmentConfiguration>(configuration);

			return services;
		}

		private static IServiceCollection AddGrpcServiceDefinition<TImplementation>(this IServiceCollection services,
			Func<TImplementation, ServerServiceDefinition> binder) 
			where TImplementation : class
		{
			services.AddSingleton<TImplementation>();
			services.AddSingleton(provider =>
				{
					var service = provider.GetRequiredService<TImplementation>();
					var interceptors = provider.GetServices<Interceptor>().ToArray();
					return binder(service).Intercept(interceptors);
				}
			);
			return services;
		}
	}
}