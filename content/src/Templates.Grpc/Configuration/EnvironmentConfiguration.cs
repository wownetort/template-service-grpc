using Serilog.Events;

namespace Templates.Grpc.Configuration
{
	public class EnvironmentConfiguration
	{
		public int Port { get; set; }
		public string Host { get; set; }
		public string MetricsServerHost { get; set; }
		public int MetricsServerPort { get; set; }
		public string PrometheusName { get; set; }

		public LogEventLevel LogLevel { get; set; }
		public bool LogsAtJsonFormat { get; set; }
	}
}