﻿using System.IO;
using System.Threading.Tasks;
using Templates.Grpc.Startup;
using Microsoft.Extensions.Hosting;

namespace Templates.Grpc
{
	internal class Program
	{
		private static async Task Main()
		{
			var host = new HostBuilder()
				.UseContentRoot(Directory.GetCurrentDirectory())
				.ConfigureHostConfiguration()
				.ConfigureAppConfiguration()
				.ConfigureAppLogging()
				.ConfigureAppServices()
				.UseGrpcListener()
				.UseMetrics()
				.Build();

			await host.RunAsync();
		}
	}
}